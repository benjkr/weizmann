import cv2

import numpy as np
# from threading import Thread


class AI:
    image = np.array([])
    faces = []
    face_cascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")

    def find_faces(self):
        """
        This function finds the faces in the frame with OpenCV.
        :return: the location of the detected faces.
        """
        return self.face_cascade.detectMultiScale(self.image, 1.2, 4)

    def has_faces(self, frame):
        """
        This function returns if there are faces in the frame, and the faces location.
        :param frame: the frame you want to check.
        :return: If there are faces detected (Boolean) and the location of the faces (array).
        """
        self.image = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        self.faces = self.find_faces()
        # thread = Thread(target=self.find_faces)

        # thread.start()

        # thread.join()

        return (len(self.faces) > 0), self.faces
