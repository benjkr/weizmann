import cv2
import numpy as np
from Feed import FEED
from PyQt5.QtCore import pyqtSignal, QObject


class BackReplace(QObject):
    update = pyqtSignal(np.ndarray)

    def __init__(self, feed: FEED):
        super().__init__()
        self.feed = feed
        self.h, self.w = None, None
        self.background_original = []
        self.background_original_copy = []

    def send_image(self, image):
        self.update.emit(image)

    def get_diff(self, frame1, frame2, debug=False):
        """
        This function returns a binary picture of the differance of two pictures.
        :param frame1: frame1.
        :param frame2: frame2.
        :return: a binary picture of the differance of two pictures.
        """
        diff = cv2.absdiff(frame1, frame2, 2)
        gray = cv2.cvtColor(diff, cv2.COLOR_BGR2GRAY)

        blur = cv2.GaussianBlur(gray, (5, 5), 0)

        _, im_th = cv2.threshold(blur, 18, 255, cv2.THRESH_BINARY)
        im_floodfill = im_th.copy()
        h, w = im_th.shape[:2]
        mask = np.zeros((h + 2, w + 2), np.uint8)

        cv2.floodFill(im_floodfill, mask, (0, 0), 255)
        fixes = cv2.bitwise_not(im_floodfill)
        merged = im_th | fixes

        dilated = cv2.dilate(merged, None, iterations=1)

        if debug:
            cv2.imshow("frame", frame1)
            cv2.imshow("im_th", im_th)
            cv2.imshow("gray", gray)
            cv2.imshow("im_floodfill", im_floodfill)
            cv2.imshow("fixes", fixes)
            cv2.imshow("dilated", dilated)
            cv2.imshow("merged", merged)
        else:
            cv2.destroyAllWindows()

        return dilated

    def green_screen_attempt(self, frame, freeze_frame, back_pic, draw=False, debug=False):
        """
        This function sends to get_diff the frame and freeze frame, calculates and replace all the pixels that are
        different.
        :param frame: The current frame.
        :param freeze_frame: The freeze frame of the environment of the user.
        :param back_pic: the background picture that will replace the back of the frame.
        """
        self.h, self.w = self.feed.get_dimensions()
        self.background_original = back_pic.copy()
        self.background_original = cv2.resize(self.background_original, (self.w, self.h), interpolation=cv2.INTER_AREA)
        self.background_original_copy = self.background_original.copy()
        self.background_original = self.background_original_copy.copy()

        diff = self.get_diff(frame, freeze_frame, debug)

        if draw:
            contours, _ = cv2.findContours(diff, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            cv2.drawContours(frame, contours, -1, (0, 255, 0), 2)

        imask = diff > 0
        self.background_original[imask] = frame[imask]

        # diff_per = len(frame[imask]) / (imask.shape[0] * imask.shape[1]) * 100
        self.send_image(self.background_original)
