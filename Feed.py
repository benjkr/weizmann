import math
import cv2
import numpy as np
import time

from Ai import AI
from PyQt5.QtCore import pyqtSignal, QObject


class FEED(QObject):
    update = pyqtSignal(np.ndarray)
    cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)
    font = cv2.FONT_HERSHEY_SIMPLEX
    ai_class = AI()

    def __init__(self):
        super().__init__()
        self.height, self.width = None, None
        self.close()

    def send_image(self, image):
        """
        The function sends a signal with an image.
        :param image: the image that will be signaled.
        """
        self.update.emit(image)

    def get_frame(self):
        """
        The function returns the current frame of the camera feed. Or returns none if the camera is not opened.
        :return: np.array
        """
        if self.cap.isOpened():
            ret, frame = self.cap.read()
            return frame
        else:
            return None

    def is_opened(self):
        """
        This function returns if the feed is open.
        :return:boolean
        """
        return self.cap.isOpened()

    def get_dimensions(self):
        """
        This function returns the dimensions of the frame coming from the camera.
        :return: tuple
        """
        ret, frame = self.cap.read()
        return frame.shape[:2]

    def draw_faces_and_profiles(self, frame, faces):
        """
        This function draws the given faces on the given frame.
        :param frame: np.array
        :param faces: np.array
        """
        for (x, y, w, h) in faces:
            cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)

    def get_freeze_frame(self, dur=2):
        """
        This function makes sure that the user will not be on screen, and returns the frame without the user.
        the function will display a timer on screen that will count to 0 and after that it will save the frame.
        :param dur: The amount of time that the timer will be.(int)
        :return: np.array
        """
        duration = dur
        current_timer = -1
        self.height, self.width = self.get_dimensions()

        start_time = time.time()
        while current_timer < duration and self.cap.isOpened():
            timer_frame = self.get_frame()
            has_human, faces = self.ai_class.has_faces(timer_frame)
            if has_human:
                start_time = time.time()
                self.draw_faces_and_profiles(timer_frame, faces)
                cv2.putText(timer_frame, "Please step out from the frame.", (0, 30), self.font, 1,
                            (255, 255, 255),
                            1)

            else:
                current_timer = time.time() - start_time
                current_timer_display = str(math.ceil(duration - current_timer))[0]
                cv2.putText(timer_frame, current_timer_display, (round(self.width / 2), round(self.height / 2)),
                            self.font, 2,
                            (255, 255, 255),
                            1)
            self.send_image(timer_frame)
            if cv2.waitKey(40) == 27:
                break
        freeze = self.get_frame()
        return freeze

    def open(self):
        """
        This function opens the camera feed.
        """
        self.cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)

    def close(self):
        """
        This function closes the camera feed.
        """
        self.cap.release()
