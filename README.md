Hello! In here you will be guided on how to run my programm.

Requirements:
1. Python 3 (https://www.python.org/downloads/release/python-3810/)
2. PyQt5 (https://pypi.org/project/PyQt5/)
3. OpenCV (https://pypi.org/project/opencv-python/)

How to run the programm:
1. Download this directory to your computer.
2. run the python file "run.py".

Thanks for running my programm!
