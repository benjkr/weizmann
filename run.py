import cv2
import numpy

from Feed import FEED
from Background_replace import BackReplace
import sys

from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5.QtGui import QImage, QIcon, QPixmap
from PyQt5.QtWidgets import QMainWindow, QAction, QApplication, QWidget, QLabel, QFileDialog, QGridLayout, QPushButton, \
    QMessageBox, QCheckBox, QGroupBox, QVBoxLayout
import os
from tips import Tips
from itertools import cycle

ALL_STYLES = """
QCheckBox {
    color: white;
}
QCheckBox::Disabled {
    color: #808080;
}
QPushButton {
    background-color: #9146fc;
    border-style: solid;
    border-color: #b594e3;
    border-width: 2px;
    border-radius: 7px;
    padding: 5px;
}
QPushButton:hover {
  text-decoration: underline;
}
QPushButton::Disabled {
    background-color: #642faf;
    color:#afafaf;
}
"""

button_label_style = """
QPushButton:hover {
  text-decoration: underline;
}
"""

"""
To build an exe file run the command: 
 'pyinstaller --add-data "icon.jfif;." --add-data "haarcascade_frontalface_default.xml;." run.py' 
in cmd.
"""


def showDialog(content):
    """
    This function display a massage on screen that tells the user that something went wrong with the uploading
    process.
    """
    msgBox = QMessageBox()
    msgBox.setIcon(QMessageBox.Information)
    msgBox.setWindowTitle("Alert")
    msgBox.setText(content)
    msgBox.setStandardButtons(QMessageBox.Ok)

    returnValue = msgBox.exec()


class CameraWidget(QWidget):
    def __init__(self, parent):
        """
        This function builds the UI section of the application, and assigns functions that will be executed when the
        buttons will be pressed.
        """
        super().__init__(parent)
        self.back_path = ""
        self.blur_back = False
        self.feed = FEED()
        self.feed.update.connect(self.update_image)
        self.background_rep = BackReplace(self.feed)
        self.background_rep.update.connect(self.update_image)
        self.parent().on_close.connect(lambda: self.feed.close())

        self.layout = QGridLayout(self)

        self.feed_groupbox = QGroupBox("Feed:")
        self.layout.addWidget(self.feed_groupbox, 0, 0)
        vbox = QGridLayout()
        self.feed_groupbox.setLayout(vbox)

        self.image_display = QLabel(self)
        self.image_display.setStyleSheet("background-color: #4c4c4f;")
        self.image_display.setText("CAMERA OUTPUT")
        # self.layout.addWidget(self.image_display, 0, 0, 2, 0)
        vbox.addWidget(self.image_display, 0, 0, 1, 2)

        self.blur_button = QPushButton("Blur back")
        self.blur_button.setStyleSheet(ALL_STYLES)
        self.blur_button.setStatusTip("Blurs the back instead of changing it.")
        # self.blur_button.setFont(QFont('Roboto', 10))
        self.blur_button.clicked.connect(self.blur_button_clicked)
        # self.layout.addWidget(self.blur_button, 2, 1)
        vbox.addWidget(self.blur_button, 2, 0)

        self.change_button = QPushButton("Change back")
        self.change_button.setStatusTip("Change the background.")
        self.change_button.setStyleSheet(ALL_STYLES)
        self.change_button.clicked.connect(self.change_button_clicked)
        # self.layout.addWidget(self.change_button, 2, 2)
        self.update_change_button()
        vbox.addWidget(self.change_button, 2, 1)

        # self.led_indicator = Led(self, on_color=Led.green, off_color=Led.red, shape=Led.circle)
        # self.led_indicator.turn_on()
        # self.led_indicator.setFocusPolicy(Qt.NoFocus)
        # vbox.addWidget(self.led_indicator)
        self.feed_label = QLabel(self)
        self.feed_label.setText("Feed status: ")
        self.feed_label.setAlignment(Qt.AlignCenter)
        vbox.addWidget(self.feed_label, 3, 0)

        self.feed_status_label = QLabel(self)
        self.feed_status_label.setText("CLOSED")
        self.feed_status_label.setAlignment(Qt.AlignCenter)
        self.feed_status_label.setStyleSheet("color:red;")
        vbox.addWidget(self.feed_status_label, 3, 1)

        self.retake_button = QPushButton("retake back")
        self.retake_button = QPushButton("retake back")
        self.retake_button.setStatusTip("retake the back picture")
        self.retake_button.clicked.connect(self.set_freeze_frame)

        self.debug_groupbox = QGroupBox("Debug:")
        self.debug_change()
        # self.debug_groupbox.setDisabled(True)
        self.debug_groupbox.setStyleSheet(ALL_STYLES)
        self.layout.addWidget(self.debug_groupbox, 1, 0)
        vbox = QVBoxLayout()
        self.debug_groupbox.setLayout(vbox)

        self.contours_checkBox = QCheckBox("Draw contours")
        # self.layout.addWidget(self.contours_checkBox, 2, 0)

        self.show_debug_checkBox = QCheckBox("Show debug")
        # self.layout.addWidget(self.show_debug_checkBox, 3, 0)

        vbox.addWidget(self.contours_checkBox)
        vbox.addWidget(self.show_debug_checkBox)

        self.upload_back_label = QPushButton()
        self.upload_back_label.setText("Click here to upload background picture")
        self.upload_back_label.setFlat(True)
        self.upload_back_label.setStyleSheet(button_label_style)
        self.upload_back_label.clicked.connect(lambda: self.parent().openFileNameDialog())

        self.layout.addWidget(self.upload_back_label, 4, 0)

        self.setLayout(self.layout)

        self.frame = []
        self.freeze_frame = []

    def show_retake_freeze_button(self):
        """
        Puts the retake button on screen.
        """
        self.layout.addWidget(self.retake_button, 0, 1)

    def hide_retake_freeze_button(self):
        """
        Hides the retake button on screen.
        """
        self.retake_button.setParent(None)

    def blur_button_clicked(self):
        """
        This function will execute when 'blur_button' is clicked.
        """
        self.blur_back = True
        self.run()

    def change_button_clicked(self):
        """
        This function will execute when 'change_button' is clicked.
        """
        self.blur_back = False
        self.run()

    def update_change_button(self):
        """
        This function will update the details of 'change_button'.
        """
        if os.path.isfile(self.back_path):
            self.change_button.setDisabled(False)
            self.change_button.setToolTip("")
        else:
            self.change_button.setDisabled(True)
            self.change_button.setToolTip("Upload image in File->'Upload background picture' OR press Ctrl+U")

    def update_image(self, image):
        """
        This function will update the image label with the image given.
        :param image: the image that will be displayed on screen.
        """
        qformat = QImage.Format_Indexed8
        if len(image.shape) == 3:
            if image.shape[2] == 4:
                qformat = QImage.Format_RGBA8888
            else:
                qformat = QImage.Format_RGB888
        outImage = QImage(image, image.shape[1], image.shape[0], image.strides[0], qformat)
        outImage = outImage.rgbSwapped()
        self.image_display.setPixmap(QPixmap.fromImage(outImage))

    def set_freeze_frame(self):
        """
        The function sets the freeze frame.
        """
        self.hide_retake_freeze_button()
        self.freeze_frame = self.feed.get_freeze_frame()
        self.show_retake_freeze_button()

    def update_back_path(self, path):
        """
        The function updates the back_path with the given path.
        :param path: the path to the background in the computer.
        """
        self.back_path = path

    def debug_change(self):
        if self.debug_groupbox.isEnabled():
            self.debug_groupbox.setDisabled(True)
            self.debug_groupbox.setTitle("Debug(Disabled):")
        else:
            self.debug_groupbox.setDisabled(False)
            self.debug_groupbox.setTitle("Debug:")

    def feed_change(self):
        if self.feed.is_opened():
            self.feed.close()
            self.feed_status_label.setText("CLOSED")
            self.feed_status_label.setStyleSheet("color:red;")
        else:
            self.feed.open()
            self.feed_status_label.setText("OPEN")
            self.feed_status_label.setStyleSheet("color:green;")

    def run(self):
        """
        This is the main function of the class.
        The function sends the current frame, the freeze frame and the background pic to BackReplace.
        This function uses classes FEED and BackReplace.
        """
        if not self.feed.is_opened():
            showDialog("The feed is closed. Please open it under the 'Tools' menu.")
            return
        # self.feed.open()
        self.set_freeze_frame()
        if self.freeze_frame is not None:
            back_pic = cv2.imread(self.back_path)

            while self.feed.is_opened():
                self.frame = self.feed.get_frame()
                if self.blur_back:
                    back_pic = cv2.blur(self.frame.copy(), (20, 20))
                self.background_rep.green_screen_attempt(self.frame, self.freeze_frame, back_pic,
                                                         draw=self.contours_checkBox.isChecked(),
                                                         debug=self.show_debug_checkBox.isChecked())
                if cv2.waitKey(40) == 27:
                    break
        cv2.destroyAllWindows()
        self.feed.close()


class MainWin(QMainWindow):
    on_close = pyqtSignal()

    def __init__(self):
        super().__init__()
        self.background_pic_path = ""
        self.window_icon = QIcon('icon.jfif')

        self.camera_widget = CameraWidget(self)
        self.setCentralWidget(self.camera_widget)

        self.tips_widget = Tips()

        self.initUI()
        self.setStyleSheet("background-color: #18181b; color:#9146fc; color:white")

    def initUI(self):
        self.upload_button = QAction('&Upload background picture', self)
        self.upload_button.setShortcut("Ctrl+U")
        self.upload_button.setStatusTip('Upload background')
        self.upload_button.triggered.connect(self.openFileNameDialog)

        self.exit_button = QAction('&Exit', self)
        self.exit_button.setShortcut("Ctrl+Q")
        self.exit_button.triggered.connect(self.closeEvent)

        self.tips_button = QAction('&Tips', self)
        self.tips_button.setShortcut("Ctrl+T")
        self.tips_button.triggered.connect(self.open_tips)

        self.debug_button = QAction('&Enable debug options', self)
        self.debug_button.setShortcut("Ctrl+D")
        self.debug_button.triggered.connect(self.change_debug_state)

        self.debug_texts = cycle(("&Disable debug options", "&Enable debug options"))

        self.feed_button = QAction('&Open feed input', self)
        self.feed_button.setShortcut("Ctrl+F")
        self.feed_button.triggered.connect(self.change_feed_state)

        self.feed_texts = cycle(("&Close feed input", "&Open feed input"))

        self.menubar = self.menuBar()
        self.fileMenu = self.menubar.addMenu('&File')
        self.fileMenu.addAction(self.upload_button)

        self.toolsMenu = self.menubar.addMenu('&Tools')
        self.toolsMenu.addAction(self.debug_button)
        self.toolsMenu.addAction(self.feed_button)

        self.helpMenu = self.menubar.addMenu('&Help')
        self.helpMenu.addAction(self.tips_button)
        self.helpMenu.addAction(self.exit_button)

        self.status_bar = self.statusBar()
        self.status_bar.showMessage("Here you will see all updates and tips.")

        self.setGeometry(300, 300, 300, 200)
        self.setWindowTitle('Background change')
        self.setWindowIcon(self.window_icon)
        self.show()

    def openFileNameDialog(self):
        """
        This function uploads from the computer the path of the selected picture.
        """
        filename = None
        filename, _ = QFileDialog.getOpenFileName(self, 'Open File', ".", "Image (*.png *.jpg *.jpeg)")
        if os.path.isfile(filename):
            self.background_pic_path = filename
            self.camera_widget.update_back_path(filename)
        elif filename == "":
            pass
        else:
            showDialog("Something went wrong. Please upload background image again.")
        self.camera_widget.update_change_button()

    def open_tips(self):
        self.tips_widget.close()
        self.tips_widget = Tips()

    def change_debug_state(self):
        self.camera_widget.debug_change()
        text = next(self.debug_texts)
        self.debug_button.setText(text)

    def change_feed_state(self):
        self.camera_widget.feed_change()
        text = next(self.feed_texts)
        self.feed_button.setText(text)

    def closeEvent(self, event):
        """
        This function will be executed when the application closes.
        """
        self.on_close.emit()
        self.tips_widget.closeEvent("")
        sys.exit(0)


def main():
    app = QApplication(sys.argv)
    main_win = MainWin()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
