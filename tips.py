import sys

from PyQt5 import QtCore
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QGridLayout, QPushButton, QStyle, QProgressBar
from time import sleep
from threading import Thread
from itertools import cycle

DEFAULT_STYLE = """
QProgressBar {
    min-width: 12px;
    max-width: 12px;
    min-height: 75%;
    max-height: 75%;
    border-radius: 6px;
    margin-right: 10px;
    margin: auto;
}
QProgressBar::chunk {
    border-radius: 6px;
    background-color: #9146fc;
}
"""


class Tips(QWidget):
    all_messages = [
        "Hello! Welcome to the background changer!\nIn this window I will try to explain to you how to use my application!",
        "To just blur the background, press the 'Blur' button.",
        "If you want to change the background with a custom image,\nplease go to File->'Upload background picture' OR press Ctrl+U. After that press 'Change back'.",
        "The feed is closed by default. If you want to open the feed, go to Tools->Open feed input.",
        "The debug options are disabled by default. If you want to enable them, go to Tools->Enable debug options."
    ]
    prog = pyqtSignal(int)

    def __init__(self, parent=None):
        """
        This function builds the UI section of the application, and assigns functions that will be executed when the
        buttons will be pressed.
        """
        super().__init__(parent)
        self.setStyleSheet("background-color: #18181b; color:#9146fc; color:white")
        self.setWindowTitle("Tips")
        self.setWindowIcon(self.style().standardIcon(QStyle.SP_MessageBoxInformation))

        self.prog.connect(self.update_prog)
        self.closed = False
        self.massages = None
        self.next_pressed = False

        self.layout = QGridLayout(self)

        self.next_button = QPushButton("Next tip")
        self.next_button.clicked.connect(self.next_button_clicked)
        self.next_button.setStyleSheet("background-color: #9146fc;color:white")
        self.layout.addWidget(self.next_button, 1, 0, 2, 0)

        self.central_label = QLabel(self)
        self.central_label.setStyleSheet("font-size: 15px")
        self.layout.addWidget(self.central_label, 0, 1)

        self.progress_bar = QProgressBar(self)
        self.progress_bar.setStyleSheet(DEFAULT_STYLE)
        self.progress_bar.setOrientation(QtCore.Qt.Vertical)
        self.layout.addWidget(self.progress_bar, 0, 0)
        self.progress_bar.setTextVisible(False)

        self.messages_setup()
        self.setLayout(self.layout)
        self.show()

        self.thread = Thread(target=self.start_progress)
        self.thread.start()

    def update_prog(self, val):
        if val == 100:
            self.display_next_message()
        self.progress_bar.setValue(val)

    def start_progress(self):
        dur_time = 7
        sleep_time = dur_time / 100
        while True and not self.closed:
            self.prog.emit(0)
            for i in range(101):
                if self.next_pressed or self.closed:
                    break
                sleep(sleep_time)
                self.prog.emit(i)
            self.next_pressed = False

    def next_button_clicked(self):
        self.next_pressed = True
        self.display_next_message()

    def display_next_message(self):
        index, message = next(self.massages)
        self.central_label.setText(str(index) + ". " + message)

    def messages_setup(self):
        temp_list = []
        for index, message in enumerate(self.all_messages, start=1):
            temp_list.append((index, message))
        self.massages = cycle(temp_list)
        self.display_next_message()

    def closeEvent(self, event):
        """
        This function will be executed when the application closes.
        """
        self.closed = True
        self.close()


def main():
    app = QApplication(sys.argv)
    main_win = Tips()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
